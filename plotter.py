#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
import time
from sys import argv

mask_file_name = "easy_mask.txt"
edges_file_name = "edges1.txt"


if len(argv) > 1:
    mask_file_name = argv[1]
if len(argv) > 2:
    edges_file_name = argv[2]



# Mask and points
mask_data = np.genfromtxt(mask_file_name, delimiter=1, dtype=int)
data2 = []
time2 = []

for iy, ix in np.ndindex(mask_data.shape):
    time2.append(ix + 0.5)
    data2.append(mask_data.shape[0] - iy - 0.5)



# All posible edges
with open(edges_file_name, "r") as f:
    lines = f.readlines();

x = []
y = []
for i in range(0, len(lines)):
    lines[i] = lines[i].split()
    x.append(time2[int(lines[i][1])])
    y.append(time2[mask_data.shape[0] - int(lines[i][0]) - 1])
    



#Drawing

plt.imshow(mask_data, extent=[0.0, mask_data.shape[1], 0.0, mask_data.shape[0]])
#plt.plot(time2, data2, 'o')

plt.plot(x, y)

plt.show()
